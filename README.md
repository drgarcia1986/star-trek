# Star Trek
This projecet is an awesome Klingon translate and star-trek characters information.
It's written in Golang only using the standard library, without
over engineering, but, with tests and attention.

## Compile
Assuming you already have a Golang environment, use Makefile to compile:
```
$ make compile
```
This command will generate a binary called `star-trek`.

## Run
Use that binary generated before:
```
$ ./star-trek NAME
```
For composed names use quotation marks, e.g.
```
$ ./star-trek "COMPOSED NAME"
```

## Run tests
To run tests use recipe `test` from Makefile:
```
$ make test
```

> Live long, and prosper.
