package character

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

type searchResponse struct {
	Characters []struct {
		Uid string `json:"uid"`
	} `json:"characters"`
}

type characterResponse struct {
	Character struct {
		CharacterSpecies []struct {
			Name string `json:"name"`
		} `json:"characterSpecies"`
	} `json:"character"`
}

var urlBase = "http://stapi.co/api/v1/rest/character"

func Species(name string) ([]string, error) {
	uid, err := getUID(name)
	if err != nil {
		return nil, err
	}
	return getSpecies(uid)
}

func getSpecies(uid string) ([]string, error) {
	res, err := http.Get(
		fmt.Sprintf("%s/?uid=%s", urlBase, uid),
	)
	if err != nil {
		return nil, fmt.Errorf("error performing request: %s", err)
	}
	cr := new(characterResponse)
	if err := json.NewDecoder(res.Body).Decode(cr); err != nil {
		return nil, fmt.Errorf("error decoding response: %s", err)
	}
	if len(cr.Character.CharacterSpecies) == 0 {
		return nil, fmt.Errorf("specie of character %s not found", uid)
	}
	species := make([]string, 0)
	for _, cs := range cr.Character.CharacterSpecies {
		species = append(species, cs.Name)
	}
	return species, nil
}

func getUID(name string) (string, error) {
	res, err := http.PostForm(
		fmt.Sprintf("%s/search", urlBase),
		url.Values{"name": {name}},
	)
	if err != nil {
		return "", fmt.Errorf("error performing request: %s", err)
	}
	sr := new(searchResponse)
	if err := json.NewDecoder(res.Body).Decode(sr); err != nil {
		return "", fmt.Errorf("error decoding response: %s", err)
	}
	if len(sr.Characters) == 0 {
		return "", fmt.Errorf("%s not found", name)
	}
	return sr.Characters[0].Uid, nil
}
