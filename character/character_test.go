package character

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSpecie(t *testing.T) {
	expected := []string{"gopher", "pythonista"}

	mux := http.NewServeMux()
	mux.HandleFunc("/search", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprint(w, `{"characters":[{"uid": "1234"}]}`)
	})
	mux.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, `
			{"character":{"characterSpecies":[
				{"name": "%s"}, {"name": "%s"}
			]}}
		`, expected[0], expected[1])
	})
	ts := httptest.NewServer(mux)
	urlBase = ts.URL

	actual, err := Species("diego")
	if err != nil {
		t.Fatal("error getting fake data:", err)
	}
	if len(expected) != len(actual) {
		t.Fatalf("expected %v, got %v")
	}
	for i := range expected {
		if expected[i] != actual[i] {
			t.Errorf("expected %s, got %s", expected[i], actual[i])
		}
	}
}
