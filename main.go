package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/drgarcia1986/startrek/alphabet"
	"github.com/drgarcia1986/startrek/character"
)

func main() {
	if len(os.Args) != 2 {
		printErrAndExit(fmt.Errorf("Usage %s NAME", os.Args[0]))
	}
	name := os.Args[1]
	specie, err := character.Species(name)
	if err != nil {
		printErrAndExit(err)
	}
	klingon := alphabet.ToKlingon(name)
	fmt.Println(strings.Join(klingon, " "))
	fmt.Println(strings.Join(specie, " "))
}

func printErrAndExit(err error) {
	fmt.Fprintln(os.Stderr, err)
	os.Exit(1)
}
