package alphabet

import (
	"strings"
	"testing"
)

func TestToKlingon(t *testing.T) {
	var testCases = []struct {
		input    string
		expected string
	}{
		{"Uhura", "0xF8E5 0xF8D6 0xF8E5 0xF8E1 0xF8D0"},
		{"c tlhqQ", "0x0020 0xF8E4 0xF8DF 0xF8E0"},
	}

	for _, tc := range testCases {
		actual := ToKlingon(tc.input)
		if strings.Join(actual, " ") != tc.expected {
			t.Errorf("expected %s, got %s", tc.expected, actual)
		}
	}
}
